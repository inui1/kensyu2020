
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>配列練習</title>
  </head>
  <body>
    <h1>配列練習ページ</h1>
      <?php

      $me_data['fruit'] = 'スイカ';
      $me_data['sport'] = 'サッカー';
      $me_data['town'] = '千葉';
      $me_data['age'] = 21;
      $me_data['food'] = 'チャーハン';

      echo $me_data['sport'];
      echo "<br>";
      echo $me_data['food'];
      echo "<br>";

      $me_data['age'] = 25; // 上書きされる

      var_dump($me_data); // 配列の中身がすべて表示される

      // ブラウザで表示させるときは<pre>で囲むと見やすく表示される
      ?>
      <pre>
      <?php var_dump($me_data);?>
      </pre>
  </body>
</html>
