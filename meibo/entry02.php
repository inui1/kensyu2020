<?php
    include('./include/statics.php');
    include('./include/header.php');
    include('./include/common.php');
    include('./include/function.php');

    $pdo = getDB();

    $error_flag = 0;      //エラーフラグ
    $param_name = "";
    if(isset($_POST['namae'])){      //POSTで取得したものに値が入っているかチェック
      $param_name = $_POST['namae']; //入っていたらparam変数に代入
    }else{                           //入っていなかったら、エラー表示でエラーフラグを立てる
      echo "名前のパラメータが入っていません";
      $error_flag = 1;
    }

    $param_from = "";
    if(isset($_POST['from'])){
      $param_from = $_POST['from'];
    }else{
      echo "出身地のパラメーターが入っていません";
      $error_flag = 1;
    }

    $param_seibetu = "";
    if(isset($_POST['seibetu'])){
      $param_seibetu = $_POST['seibetu'];
    }else{
      echo "性別のパラメータが入っていません";
      $error_flag = 1;
    }

    $param_age = "";
    if(isset($_POST['age'])){
      $param_age = $_POST['age'];
    }else{
      echo "年齢のパラメータが入っていません";
      $error_flag = 1;
    }

    $param_section = "";
    if(isset($_POST['section'])){
      $param_section = $_POST['section'];
    }else{
      echo "所属部署のパラメータが入っていません";
      $error_flag = 1;
    }

    $param_grade = "";
    if(isset($_POST['grade'])){
      $param_grade = $_POST['grade'];
    }else{
      echo "役職のパラメータが入っていません";
      $error_flag = 1;
    }

    if($error_flag == 0){                           //エラーフラグが立っていない場合、DB処理を行う
      $query_str = "INSERT INTO member
                   (name, pref, seibetu,age,section_ID,grade_ID)
                   VALUES ('" . $param_name . "','" . $param_from . "','" . $param_seibetu . "','" . $param_age . "','" . $param_section . "','" . $param_grade . "')";
      try{
        $sql = $pdo->prepare($query_str);
        $sql -> execute();          //SQLを実行
        $id = $pdo->lastInsertId('member_ID');
      }catch(Exception $ex){        //エラー処理
          echo $ex->getmassage();
      }

      if($id!=""){                   //idが入っていたらリダイレクト処理
        $url ="detail01.php?member_ID=" . $id;
        header('Location:' . $url);
        exit;
      }else{                         //入ってない場合は、エラー表示
          echo "IDを取得できませんでした。";
          echo "<a href = './index.php'>トップページへ戻ります。</a>";
      }
    }else{   //エラーフラグが立ってる場合は、エラー表示
      echo "登録できませんでした。";
      echo "<a href = './index.php'>トップページへ戻ります。</a>";
    }
?>
