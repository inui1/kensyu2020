<?php
    include('./include/statics.php');    //外部ファイルの読み込み
    include('./include/header.php');
    include('./include/common.php');
    include('./include/function.php');

    $pdo = getDB();                     //DBへの接続
    $error_flag = 0;                    //エラーフラグ定義
    $param_id="";

    if(isset($_POST['member_ID'])&&($_POST['member_ID'])!=""){
      $param_id = $_POST['member_ID'];                             //POSTで取得したmember_IDに値が入っている場合、代入する
    }else{
      echo "IDのパラメータが入っていません";
      $error_flag = 1;                                              //入っていない場合は、エラー表示してフラグを立てる
    }

    if($error_flag == 0){                                          //エラーフラグが立っていない場合
      $query_str = "DELETE FROM member WHERE member.member_ID =" . $param_id;
      try{
        $sql = $pdo->prepare($query_str);
        $sql -> execute();          //SQLを実行
      }catch(Exception $ex){
          echo $ex->getmassage();
      }
    }

    echo $query_str;

    if($ex == ""){                                              //SQL実行時にエラーが出なければトップ画面へリダイレクト
    $url ="index.php";
    header('Location:' . $url);
    exit;
    }else{
    echo "削除できませんでした。";
    echo "<a href = './index.php'>トップページへ戻ります。</a>";
    }
?>
