<!DOCTYPE html>
<html>
  <div id="all_layout">
    <link rel="stylesheet" type="text/css" href="./include/layout.css" />
    <?php           //外部ファイルの読み込み
      include('./include/statics.php');
      include('./include/header.php');
      include('./include/common.php');
      include('./include/function.php');
      include('./include/footer.php');
    ?>

  <script type="text/javascript">

  function clearDate(){               //入力した値をリセットする
    document.entry.namae.value="";
    document.entry.from.value="";
    document.entry.seibetu.value="1";
    document.entry.age.value="";
    document.entry.section.value="1";
    document.entry.grade.value="1";
  }

  </script>
  <script src="./include/function.js"></script>

  <body>
    <center>
    <form method="post" action="entry02.php" name="entry">
       <table border='1'>
         <tr>
           <td class="th_bgc"><b>名前：</b></td>
           <td><input type="text" name="namae" size="15" value="">
           </td>
         </tr>
         <tr>
           <td class="th_bgc"><b>出身地：</b></td>
           <td>
             <select name="from">
               <option value="">都道府県</option>
               <?php
                  foreach($pref_array as $key => $value){         //static.phpにある$pref_arrayをforeachで回す
                    echo "<option value = '" . $key . "'>" . $value . "</option>";//valueに番号を入れ、件名を表示させる
                  }
               ?>
             </select>
           </td>
         </tr>
         <tr>
           <td class="th_bgc"><b>性別：</b></td>
           <td>
               <?php
                foreach($gender_array as $key => $sex){        //static.phpにある$gender_arrayをforeachで回す
                  if($key == "1"){                             //$keyの値が1だった場合、チェックを入れる
                    echo"<label><input type='radio' name='seibetu' value='" . $key . "'checked>" . $sex . "</label>";
                  }else{
                    echo"<label><input type='radio' name='seibetu' value='". $key . "'>" . $sex . "</label>";
                  }
                }
               ?>
           </td>
         </tr>
         <tr>
             <td class="th_bgc"><b>年齢：</b></td>
             <td><input type="number" name="age" size="15" min="1" max="99" value=''>
             </td>
         </tr>
         <tr>
           <td class="th_bgc"><b>所属部署：</b></td>
           <td>
             <?php
                $section_result = getSection();
                foreach($section_result as $each){ //取得した部署名をforeach文で$eachへ入れる
                  if( $each['ID'] == "1"){         //$each[ID]が1だった場合、チェックを入れる
                    echo "<label><input type='radio' name='section' value='" . $each['ID'] . "'checked>" . $each['section_name'] . "</label>";
                  }else{
                    echo "<label><input type='radio' name='section' value='". $each['ID'] . "'>".$each['section_name'] . "</label>";
                  }
                }
             ?>
           </td>
         </tr>

         <tr>
           <td class="th_bgc"><b>役職：</b></td>
           <td>
             <?php
                $grade_result = getGrade();
                foreach($grade_result as $each){ //取得した部署名をforeach文で$eachへ入れる
                  if($each['ID'] == "1"){        //$each[ID]が1だった場合、チェックを入れる
                    echo "<label><input type='radio' name='grade' value='". $each['ID'] . "' checked>".$each['grade_name'] . "</label>";
                  }else{
                    echo "<label><input type='radio' name='grade' value='". $each['ID'] . "'>".$each['grade_name'] . "</label>";
                  }
                }
             ?>
           </td>
         </tr>
       </table>
      <br>
      <input type="button" value="登録" onClick='check_Date();'>
      <input type="button" value="リセット" onClick='clearDate();'>
    </form>
    </center>
  </body>
  </div>
</html>
