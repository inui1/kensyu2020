
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>配列練習</title>
  </head>
  <body>
    <h1>配列練習ページ</h1>
      <?php

      $me_data['fruit'] = 'スイカ';
      $me_data['sport'] = 'サッカー';
      $me_data['town'] = '千葉';
      $me_data['age'] = 21;
      $me_data['food'] = 'チャーハン';

      echo $me_data['sport'];
      echo "<br>";
      echo $me_data['food'];
      echo "<br>";

      foreach($me_data as $each){
        echo $each . "<br />";
      }

      foreach($me_data as $key => $value){
        echo $key . ":" . $value . "<br />";
      }
      ?>
  </body>
</html>
