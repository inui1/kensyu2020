<!DOCTYPE html>
<?php
if(is_uploaded_file($_FILES["csvfile"]["tmp_name"])){      //ファイルの読み込み
    $file_tmp_name = $_FILES["csvfile"]["tmp_name"];
    $file_name = $_FILES["csvfile"]["name"];

    if(pathinfo($file_name, PATHINFO_EXTENSION) != 'csv'){  //拡張子がcsv出ない場合
        $err_msg = 'CSVファイルのみ対応しています。';         //エラー表示(上手く表示されない・・・)
    }else{                                                            //拡張子がcsvならば
        if(move_uploaded_file($file_tmp_name,"/tmp/" . $file_name)){
            chmod("/tmp/" . $file_name,0644);                //権限を設定する
            $msg = $file_name . "をアップロードしました。";
            $file = '/tmp/'.$file_name;
            $fp = fopen($file,"r");                          //読み取りでファイルを開く

            while(($data = fgetcsv($fp,0,",")) !== FALSE){    //csvファイルを読み込む
                $asins[] = $data;                             //読み込んだ値を配列に代入する
            }
            fclose($fp);                                     //ファイルを閉じる
            unlink('/tmp/'.$file_name);                      //ファイルを削除する
        }else{
            $err_msg = "ファイルをアップロードできません。";
        }
    }
}else{
    $err_msg = "ファイルが選択されていません。";
}
?>

<html>
  <head>
    <meta caraset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>csvインポート</title>
  </head>

  <body>
    <!-- 外部ファイルの読み込み -->
    <?php include("./include/header.php"); ?>
    <hr>
    <pre>
      <?php var_dump($asins); ?>
    </pre>

    <table>
      <?php
        foreach($asins as $each){                           //読み込んだ値をforeachで回す
          echo "<tr>";
            foreach($each as $date){
              echo "<td>" . mb_convert_encoding($data,'UTF-8','sjis-win') . "</td>";  //データをUTF-8にエンコードする
            }
          echo "</tr>";
        }
      ?>
    </table>

    <?php
      include("./include/function.php");     //外部ファイルの読み込み
      $pod = getDB();

      $array_fields=$asins[0];                 //配列の定義
      $query_str_base = "INSERT INTO member (";
      foreach($array_fields as $each){
          $query_str_base .= $each . ",";
      }
      $query_str_base = substr($query_str_base, 0, -1);
      $query_str_base .= ") VALUES (";

      array_splice($asins, 0, 1);
      echo "<hr>削除後の配列";
      var_dump($asins);
      echo "<hr/>";
      foreach($asins as $each){
          $query_str = $query_str_base;
          foreach($each as $data){
              $query_str .= "'" . mb_convert_encoding($data, 'UTF-8', 'sjis-win') . "',";
          }
          $query_str = substr($query_str, 0, -1);
          $query_str .= ")";                        //SQL文
          echo $query_str . "<hr/>";
          echo $query_str;
          try{                                      //SQLの実行
              $sql = $pod->prepare($query_str);
              $sql->execute();
          }catch(PDOException $e){
            print $e->getMessage();
          }
      }
      $url ="index.php";                          //トップページへのリダイレクト処理
      header('Location:' . $url);
      exit;
    ?>
  </body>
</html>
