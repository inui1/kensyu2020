<?php                                     //外部ファイルの読み込み
    include('./include/statics.php');
    include('./include/header.php');
    include('./include/common.php');
    include('./include/function.php');

    $pdo = getDB();                //DB接続
    $error_flag = 0;               //エラーフラグ定義
    $param_id="";

    if(isset($_POST['member_ID'])&&($_POST['member_ID'])!=""){    //POSTで取得したmember_IDに値が入っていれば
      $param_id = $_POST['member_ID'];                            //変数に代入する
    }else{
      echo "IDのパラメータが入っていません";                         //入っていなければ、エラーフラグを立てる
      $error_flag = 1;
    }

    $param_name = "";
    if(isset($_POST['namae'])&&($_POST['namae'])!=""){
      $param_name = $_POST['namae'];
    }else{
      echo "名前のパラメータが入っていません";
      $error_flag = 1;
    }

    $param_from = "";
    if(isset($_POST['from'])&&($_POST['from'])!=""){
      $param_from = $_POST['from'];
    }else{
      echo "出身地のパラメーターが入っていません";
      $error_flag = 1;
    }

    $param_seibetu = "";
    if(isset($_POST['seibetu'])&&($_POST['seibetu'])!=""){
      $param_seibetu = $_POST['seibetu'];
    }else{
      echo "性別のパラメータが入っていません";
      $error_flag = 1;
    }

    $param_age = "";
    if(isset($_POST['age'])&&($_POST['age'])!=""){
      $param_age = $_POST['age'];
    }else{
      echo "年齢のパラメータが入っていません";
      $error_flag = 1;
    }

    $param_section = "";
    if(isset($_POST['section'])&&($_POST['section'])!=""){
      $param_section = $_POST['section'];
    }else{
      echo "所属部署のパラメータが入っていません";
      $error_flag = 1;
    }

    $param_grade = "";
    if(isset($_POST['grade'])&&($_POST['grade'])!=""){
      $param_grade = $_POST['grade'];
    }else{
      echo "役職のパラメータが入っていません";
      $error_flag = 1;
    }

    if($error_flag == 0){                               //エラーフラグが立っていない場合
        $query_str = "UPDATE member as m SET name = '"
                      . $param_name . "',m.pref = '".  $param_from . "',m.seibetu = '"
                      . $param_seibetu . "',m.age = '" . $param_age . "',m.section_ID ='"
                      . $param_section . "',m.grade_ID='" . $param_grade . "'
                      WHERE m.member_ID = " . $param_id;
        try{
            $sql = $pdo->prepare($query_str);
            $sql -> execute();          //SQLを実行
        }catch(Exception $ex){          //DB操作でエラーが出た場合
          echo $ex->getmassage();       //メッセージを表示する
        }

        if($param_id!=""){              //idが入っている場合
            $url ="detail01.php?member_ID=" . $param_id;  //登録した社員の詳細画面へリダイレクトする
            header('Location:' . $url);
            exit;
        }else{                         //idが入っていない場合
            echo "登録できませんでした。";
            echo "<a href = './index.php'>トップページへ戻ります。</a>";
        }
    }else{                                //エラーフラグが立っている場合
      echo "登録できませんでした。";
      echo "<a href = './index.php'>トップページへ戻ります。</a>";
    }
?>
