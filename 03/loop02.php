
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ループ練習</title>
  </head>
  <body>
    <h1>ループ練習ページ</h1>
      <form method="get" action="">
          <input type="text" name="row" size="5" maxlength="2">
          行☓
          <input type="text" name="cols" size="5" maxlength="2">
          列
      <br>
      <input type="submit" value="送信">
      <input type="reset" value="取り消し">
      </form>
      <br>

    <table border="1">
      <?php
      for($i=0;$i <$_GET['cols'];$i++){     //列のループ
        echo "<tr>";
        for($n=0; $n<$_GET['row'];$n++){  //行のループ
          echo "<td>テスト</td>";
        }
        echo "</tr>";
      }
      ?>
    </table>
  </body>
</html>
