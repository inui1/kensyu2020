
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>配列練習</title>
  </head>
  <body>
    <h1>配列練習ページ</h1>
      <?php
        $wepon = array("大剣","太刀","片手剣","双剣","ランス","ガンランス","スラッシュアックス","チャージアックス","操蟲棍");
        $wepon[9] = "穿龍棍";
        for($i=0;$i<count($wepon);$i++){
          echo $wepon[$i] . "<br />";
        }
        echo "<hr>";
        foreach($wepon as $each){
          echo $each . "<br />";
        }

        $needle="ガンランス";
        if(in_array($needle,$wepon)){
          echo $needle . "がありました。";
        }else{
          echo $needle . "がありませんでした。";
        }

      ?>
  </body>
</html>
