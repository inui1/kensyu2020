
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>第二回課題、フォーム部品練習</title>
  </head>
  <body>
    <h1>消費税計算ページ</h1>
      <form method="get" action="">
        <table border="1">
          <tr>
            <td><b>商品名</b></td>
            <td><b>価格（単位：円、税抜き）</b></td>
            <td><b>個数</b></td>
            <td><b>税率</b></td>
          </tr>
          <tr>
            <td><input type="text" name="syohin[0]" size="15" maxlength="10"</td>
            <td><input type="text" name="kakaku[0]" size="15" maxlength="10"</td>
            <td><input type="text" name="kosu[0]" size="5" maxlength="3">個</td>
            <td>
              <input type="radio" name="tax[0]" value="1.08">0.8%
              <input type="radio" name="tax[0]" value="1.1">10%
            </td>
          </tr>
          <tr>
            <td><input type="text" name="syohin[1]" size="15" maxlength="10"</td>
            <td><input type="text" name="kakaku[1]" size="15" maxlength="10"</td>
            <td><input type="text" name="kosu[1]" size="5" maxlength="3">個</td>
            <td>
              <input type="radio" name="tax[1]" value="1.08">0.8%
              <input type="radio" name="tax[1]" value="1.1">10%
            </td>
          </tr>
          <tr>
            <td><input type="text" name="syohin[2]" size="15" maxlength="10"</td>
            <td><input type="text" name="kakaku[2]" size="15" maxlength="10"</td>
            <td><input type="text" name="kosu[2]" size="5" maxlength="3">個</td>
            <td>
              <input type="radio" name="tax[2]" value="1.08">0.8%
              <input type="radio" name="tax[2]" value="1.10">10%
            </td>
          </tr>
          <tr>
            <td><input type="text" name="syohin[3]" size="15" maxlength="10"</td>
            <td><input type="text" name="kakaku[3]" size="15" maxlength="10"</td>
            <td><input type="text" name="kosu[3]" size="5" maxlength="3">個</td>
            <td>
              <input type="radio" name="tax[3]" value="1.08">0.8%
              <input type="radio" name="tax[3]" value="1.1">10%
            </td>
          </tr>
          <tr>
            <td><input type="text" name="syohin[4]" size="15" maxlength="10"</td>
            <td><input type="text" name="kakaku[4]" size="15" maxlength="10"</td>
            <td><input type="text" name="kosu[4]" size="5" maxlength="3">個</td>
            <td>
              <input type="radio" name="tax[4]" value="1.08">0.8%
              <input type="radio" name="tax[4]" value="1.1">10%
            </td>
          </tr>

        </table>
        <br>
      <input type="submit" value="送信">
      <input type="reset" value="取り消し">
      </form>

    <?php
      echo "<table border='1'>";
      echo "<tr>
          <td><b>商品名</b></td>
          <td><b>価格（単位：円、税抜き）</b></td>
          <td><b>個数</b></td>
          <td><b>税率</b></td>
          <td><b>小計（単位：円）</b></td>
          </tr>";

      $gokei=0;
      for($i=0; $i<=4; $i++){
        $tax=$_GET['tax'][$i];
        $kakaku=$_GET['kakaku'][$i];
        $price=$kakaku * $tax;
        $gokei=$gokei + $price;

        if($tax==1.08){
          $tax=0.8;
        }else{
          $tax=10;
        }

      echo "<tr>
          <td>" . $_GET['syohin'][$i]. "</td>
          <td>" . $_GET['kakaku'][$i] . "</td>
          <td>" . $_GET['kosu'][$i] . "</td>
          <td>" . $tax . "</td>
          <td>" . $price . "</td>
      </tr>";
      }
      echo "<tr>
          <td colspan='4'>合計</td>;
          <td>" . $gokei . "</td>
          </tr>";
      echo "</table>";
    ?>
  </body>
</html>
