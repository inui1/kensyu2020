<!DOCTYPE html>
<?php
if(is_uploaded_file($_FILES["jsonfile"]["tmp_name"])){     //ファイルの読み込み
  $file_tmp_name = $_FILES["jsonfile"]["tmp_name"];
  $file_name = $_FILES["jsonfile"]["name"];

  if(pathinfo($file_name,PATHINFO_EXTENSION) != 'json'){   //拡張子がjsonではない場合
    $err_msg = 'jsonファイルのみ対応しています。';
  }else{
    if(move_uploaded_file($file_tmp_name,"/tmp/" . $file_name)){
      chmod("/tmp/" . $file_name,0644);        //ファイルの権限指定
      $msg = $file_name . "をアップロードしました。";
      $file = '/tmp/'.$file_name;

      $temp_json_str = file_get_contents($file);//ファイルの内容をtemp_json_strに代入する

      $temp_json = json_decode($temp_json_str,true);  //jsonファイルの出コード処理
      unlink('/tmp/'.$file_name);     //ファイルの削除
    }else{
      $err_msg = "ファイルをアップロードできません。";
    }
  }
}else{
  $err_msg = "ファイルが選択されていません。";
}
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>jsonインポート</title>
  </head>

  <body>
    <?php include("./include/header.php"); ?>
    <hr>
    <pre>
      <?php var_dump($temp_json); ?>
    </pre>

    <?php
      include("./include/function.php");     //外部ファイルの読み込み
      $pod= getDB();                         //DB接続

      $array_fields=$temp_json[0];
      $query_str_base = "INSERT INTO member (";
      foreach($array_fields as $key => $value){
          $query_str_base .= $key . ",";
      }
      $query_str_base = substr($query_str_base, 0, -1);
      $query_str_base .= ") VALUES (";

      echo "<hr>削除後の配列";
      var_dump($temp_json);
      echo "<hr/>";

      foreach($temp_json as $each){
          $query_str = $query_str_base;
          foreach($each as $key=>$data){
              $query_str .= "'" . $data . "',";
              echo $data;
          }
          $query_str = substr($query_str, 0, -1);
          $query_str .= ")";                              //SQL文

        // echo $query_str . "<hr/>";

        try{
          $sql = $pod->prepare($query_str);
          $sql->execute();                                 //SQL実行
        }catch(PDOException $e){
            print $e->getMessage();
        }
      }
      $url ="index.php";                                   //トップ画面へリダイレクト
      header('Location:' . $url);
      exit;
    ?>
  </body>
</html>
