<!DOCTYPE html>
<html>
  <div id="all_layout">
  <link rel="stylesheet" type="text/css" href="./include/layout.css" />
  <?php       //外部ファイルの読み込み
    include('./include/statics.php');
    include('./include/header.php');
    include('./include/common.php');
    include('./include/function.php');
    include('./include/footer.php');
  ?>

  <script type="text/javascript">

    function clearDate(){                       //データのリセット処理
        document.entry.namae.value="";
        document.entry.from.value="";
        document.entry.seibetu.value="1";
        document.entry.age.value="";
        document.entry.section.value="1";
        document.entry.grade.value="1";
    }

  </script>
  <script src="include/function.js"></script>
  <body>
    <center>
    <form method="post" action="entry_update02.php" name="entry">
      <?php
        $query_str = "SELECT member_ID,name,pref,seibetu,age,section_id,section_name,grade_id,grade_name
                      FROM member AS m
                      LEFT JOIN section1_master AS sm
                      ON m.section_ID = sm.ID
                      LEFT JOIN grade_master AS gm
                      ON m.grade_ID = gm.ID
                      WHERE m.member_ID ='" . $_GET['member_ID'] . "'";  //全文検索
        $pdo = getDB();        //DB接続
        if(isset($_GET['member_ID'])){   //IDの有無をチェック
        $sql = $pdo->prepare($query_str);//prepareクエリを呼び出す
        $sql -> execute();          //SQLを実行
        $result = $sql->fetchAll(); //実行結果を取得
        }else{                      //IDがない場合はエラー表示
          echo "idがありません。";
          echo "<a href = './index.php'>トップページへ戻ります。</a>";
        }
        $id = $result[0]['member_ID'];
      ?>
      <!-- 社員情報表示 -->
      <table border='1'>
        <tr>
           <td class="th_bgc"><b>社員ID：</b></td>
           <td><?php echo $result[0]['member_ID'];?>
           </td>
        </tr>
        <tr>
        <tr>
           <td class="th_bgc"><b>名前：</b></td>
           <td><input type="text" name="namae" size="15" value=<?php echo $result[0]['name'];?>>
           </td>
        </tr>
        <tr>
           <td class="th_bgc"><b>出身地：</b></td>
           <td>
             <select name="from">
               <?php
                foreach($pref_array as $key => $value){     //static.phpの$pref_arrayをforeach文で回して値と件名を表示
                  echo "<option value = '" . $key . "'";
                  if($result[0]['pref']==$key){echo "selected";}//DBから取ってきた値と同じならselected処理をする
                  echo ">" . $value . "</option>";
                }
               ?>
             </select>
           </td>
        </tr>
        <tr>
           <td class="th_bgc"><b>性別：</b></td>
           <td>
             <?php
                foreach($gender_array as $key => $sex){
                  if($key==$result[0]['seibetu']){         //DBから取ってきた値と同じならchecked処理をする
                    echo"<label><input type='radio' name='seibetu' value='". $key . "'checked>" . $sex . "</label>";
                  }else{
                    echo"<label><input type='radio' name='seibetu' value='". $key . "'>" . $sex . "</label>";
                  }
                }
             ?>
           </td>
        </tr>
        <tr>
           <td class="th_bgc"><b>年齢：</b></td>
           <!-- 2桁入力とし、typeをnumberにすることにより入力を数字のみとする                          -->
           <td><input type="number" name="age" size="2" max="99" min="1" value=<?php echo $result[0]['age'];?>>
           </td>
        </tr>
        <tr>
           <td class="th_bgc"><b>所属部署：</b></td>
           <td>
             <?php
                $section_result = getSection();
                foreach($section_result as $each){ //取得した部署名をforeach文で$eachへ入れる
                  if($each['ID'] == $result[0]['section_id']){//もし取ってきた部署IDと同じであったら、表示させる(checked処理)
                    echo "<label><input type='radio' name='section' value='". $each['ID'] . "' checked>".$each['section_name'] . "</label>";
                  }else{
                    echo "<label><input type='radio' name='section' value='". $each['ID'] . "'>".$each['section_name'] . "</label>";
                  }
                }
             ?>
           </td>
        </tr>

        <tr>
           <td class="th_bgc"><b>役職：</b></td>
           <td>
             <?php
                $grade_result = getGrade();
                foreach($grade_result as $each){ //取得した部署名をforeach文で$eachへ入れる
                  if($each['ID'] == $result[0]['grade_id']){//もし取ってきた役職IDと同じであったら、表示させる(checked処理)
                    echo "<label><input type='radio' name='grade' value='". $each['ID'] . "' checked>".$each['grade_name'] . "</label>";
                  }else{
                    echo "<label><input type='radio' name='grade' value='". $each['ID'] . "'>".$each['grade_name'] . "</label>";
                  }
                }
             ?>
           </td>
        </tr>
      </table>
      <br>
      <input type="hidden" name="member_ID" value='<?php echo $id;?>'>
      <input type="button" value="登録" onClick='check_Date()'>
      <input type="button" value="リセット" onClick='clearDate()'>
    </form>
    </center>
  </body>
  </div>
</html>
