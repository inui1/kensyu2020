<!DOCTYPE html>
<html>
<div id="all_layout">
<link rel="stylesheet" type="text/css" href="./include/layout.css" />
  <?php                                   //外部ファイルの読み込み
    include('./include/statics.php');
    include('./include/header.php');
    include('./include/common.php');
    include('./include/function.php');
    include('./include/footer.php');
  ?>
<script type="text/javascript">

  // 削除ダイアログ処理
  function delDate(){
    if(window.confirm('削除します。')){
        document.delete.submit();
    }else{
        window.alert('キャンセルしました。');
    }
  }

</script>

  <body>
    <center>
    <?php
      $pdo = getDB();                     //DB接続

      $query_str = "SELECT member_ID,name,pref,seibetu,age,section_name,grade_name
                    FROM member AS m
                    LEFT JOIN section1_master AS sm
                    ON m.section_ID = sm.ID
                    LEFT JOIN grade_master AS gm
                    ON m.grade_ID = gm.ID
                    WHERE m.member_ID ='" . $_GET['member_ID'] . "'";                                 //全文検索

      if(isset($_GET['member_ID'])){      //GETで取得されているかの確認
        $sql = $pdo->prepare($query_str);//prepareクエリを呼び出す
        $sql -> execute();          //SQLを実行
        $result = $sql->fetchAll(); //実行結果を取得

        $data_check = count($result);   //データの個数を確認

        if($data_check == 1){            //データが1つの場合テーブル作成をする
          $id = $result[0]['member_ID'];
          echo "<table border='1' id = 'detail_table'>";
          echo"<tr><th class='th_bgc'>社員ID</th>
               <td class='detail_td'>" . $result[0]['member_ID'] . "</td>
               </tr>
               <tr><th class='th_bgc'>名前</th>
               <td class='detail_td'>" . $result[0]['name'] . "</td>
               </tr>
               <tr><th class='th_bgc'>出身地</th>
               <td class='detail_td'>" . $pref_array[$result[0]['pref']] . "</td>
               </tr>
               <tr><th class='th_bgc'>性別</th>
               <td class='detail_td'>" . $gender_array[$result[0]['seibetu']] . "</td>
               </tr>
               <tr><th class='th_bgc'>年齢</th>
               <td class='detail_td'>" . $result[0]['age'] . "</td>
               </tr>
               <tr><th class='th_bgc'>所属部署</th>
               <td class='detail_td'>" . $result[0]['section_name'] . "</td>
               </tr>
               <tr><th class='th_bgc'>役職</th>
               <td class='detail_td'>" . $result[0]['grade_name'] . "</td>
               </tr>";
          echo "</table>";

          echo "<br>";
          echo "<div id = 'detail_button'>";
          echo "<div id = 'd_1'>";
          echo "<form method='get' action='entry_update01.php'>";        //ボタン作成
          echo    "<input type='hidden' name='member_ID' value='" .  $id . "'>";
          echo    "<input type='submit' value='編集'>";
          echo    "</form></div>";

          echo "<div id = 'd_2'>
                <form method='post' action='delete01.php' name = 'delete'>";
          echo    "<input type='hidden' name='member_ID' value='" . $id . "'>";
          echo    "<input type='button' value='削除' onClick='delDate()'>";
          echo    "</form>
                </div>";
          echo "</div>";
        }elseif($data_check == 0){                             //データが０件の場合
          echo "データがありません。";
          echo "<a href = './index.php'>トップページへ戻ります。</a>";
        }else{                                                //データが2件以上入っている場合
          echo "データが壊れている可能性があります。";
          echo "<a href = './index.php'>トップページへ戻ります。</a>";
        }
      }else{                                                  //IDが取得できない場合
        echo "idが入っていません。";
        echo "<a href = './index.php'>トップページへ戻ります。</a>";
      }
    ?>
    </center>
  </body>
</div>
</html>
