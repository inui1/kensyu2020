<!DOCTYPE html>
<html>
  <div id="all_layout">
      <link rel="stylesheet" type="text/css" href="./include/layout.css" />
        <?php         //外部ファイルの取り込み
          include('./include/header.php');
          include('./include/common.php');
          include('./include/function.php');
          include('./include/footer.php');
          include('./include/statics.php');
        ?>
      <script type="text/javascript">

      function clearDate(){     //入力データのリセット
        document.index.namae.value = "";
        document.index.seibetu.value = "0";
        document.index.section.value = "0";
        document.index.grade.value = "0";
      }

      function checkSpace(){    //空白を埋める
        var temp_name = document.index.namae.value;
        document.index.namae.value = temp_name.replace(/\s+/g,"");
      }
      </script>

  <body>
    <?php
      $check_namae = "";                //GETで取ってきたものに値が入っているかのチェック
      if(isset($_GET['namae'])){        //入っていたらcheck変数に代入。以降はこの変数を使用する。
        $check_namae = $_GET['namae'];
      }

      $check_seibetu = "";
      if(isset($_GET['seibetu']) && $_GET['seibetu'] != ""){
        $check_seibetu = $_GET['seibetu'];
      }

      $check_section = "";
      if(isset($_GET['section']) && $_GET['section'] != ""){
        $check_section = $_GET['section'];
      }

      $check_grade = "";
      if(isset($_GET['grade']) && $_GET['grade'] != ""){
        $check_grade = $_GET['grade'];
      }
    ?>
    <form method="get" action="" name="index">
      <div id="search_rayout">
        <table width="700px">
          <tr>
            <td><b>名前：</b></td>
            <td><input type="text" name="namae" size="35" maxlength="30" value = "<?php echo $check_namae;?>">
            </td>
          </tr>
          <tr>
            <td><b>性別：</b></td>
            <td>
              <select name="seibetu">
                <option value="0">すべて</option>
                <option value="1"<?php if($check_seibetu == "1"){echo " selected";}?>>男</option>
                <option value="2"<?php if($check_seibetu == "2"){echo " selected";}?>>女</option>
              </select>
            </td>
            <td><b>部署：</b></td>
            <td>
              <select name="section">
                <option value="0">すべて</option>
                <?php
                    $section_result = getSection();    //DBから部署名呼び出し
                    foreach($section_result as $each){ //取得した部署名をforeach文で$eachへ入れる
                      if($check_section == $each['ID']){//もし取ってきた部署IDと同じであったら、表示させる(select処理)
                        echo "<option value = '" . $each['ID'] . " selected'>". $each['section_name'] . "</option>";
                      }else{
                        echo "<option value = '" . $each['ID'] . "'>". $each['section_name'] . "</option>";
                      }
                    }
                ?>
              </select>
            </td>
            <td><b>役職：</b></td>
            <td>
              <select name="grade">
                <option value="0">すべて</option>
                <?php
                    $grade_result = getGrade();        //DBから役職名呼び出し
                    foreach($grade_result as $each){   //取得した役職名をforeach文で$eachへ入れる
                      if($check_grade == $each['ID']){ //もし取ってきた役職IDと同じであったら表示させる(select処理)
                          echo "<option value = '" . $each['ID'] . " selected'>". $each['grade_name'] . "</option>";
                      }else{
                          echo "<option value = '" . $each['ID'] . "'>". $each['grade_name'] . "</option>";
                      }
                    }
                ?>
              </select>
            </td>
          </tr>
        </table>
      </div>
      <br>
      <div id="search_button">
          <!-- 検索時に入力チェック関数を呼ぶ -->
          <input type="submit" value="検索" onClick='checkSpace();'>
          <!-- 入力のリセット関数を呼ぶ -->
          <input type="button" value="リセット" onClick='clearDate();'>
      </div>
    </form>
    <hr />
    <?php
      $pdo = getDB();     //DB接続

      $query_str = "SELECT member_ID,name,age,seibetu,pref,section_name,grade_name
                    FROM member AS m
                    LEFT JOIN section1_master AS sm
                    ON m.section_ID = sm.ID
                    LEFT JOIN grade_master AS gm
                    ON m.grade_ID = gm.ID
                    WHERE 1=1 ";                                 //全文検索

      if(isset($_GET['namae']) && $_GET['namae'] !=""){
        $query_str .= " AND m.name LIKE '%" . $_GET['namae'] . "%'";
      }                                                         //名前入力時の追加SQL文

      if(isset($_GET['seibetu']) && $_GET['seibetu'] !="" && $_GET['seibetu'] != "0"){
        $query_str .= " AND m.seibetu ='" . $_GET['seibetu'] . "'";
      }                                                         //性別選択時の追加SQL文

      if(isset($_GET['section']) && $_GET['section'] !="" && $_GET['section'] != "0"){
        $query_str .= " AND sm.ID ='" . $_GET['section'] . "'";
      }                                                         //部署選択時の追加SQL文

      if(isset($_GET['grade']) && $_GET['grade'] !="" && $_GET['grade'] != "0"){
        $query_str .= " AND gm.ID ='" . $_GET['grade'] . "'";
      }                                                         //役職選択時の追加SQL文

      $sql = $pdo->prepare($query_str);//prepareクエリを呼び出す
      $sql -> execute();          //SQLを実行
      $result = $sql->fetchAll(); //実行結果を取得

      if(array_key_exists('csv',$_GET) && $_GET['csv']=="1"){   //csvファイル、エクスポート処理
          try{                                //CSV形式で情報をファイルに出力のための準備
              $csvFileName = '/tmp/' . time() . rand() . '.csv';
              $res = fopen($csvFileName, 'w');
              if ($res === FALSE) {
                  $err_msg='ファイルの書き込みに失敗しました。';
              }
              $dataList = array(              // データ一覧。この部分を引数とか動的に渡す
                          array('社員番号','名前','年齢','性別','出身','役職','部署')
                          );

              foreach($result as $row){       // ここで検索結果を配列にPushしていく
                  $temp_array = array($row['member_ID'],$row['name'],$row['age'],$gender_array[$row['seibetu']],$pref_array[$row['pref']],
                                      $row['grade_name'],$row['section_name']);
                  array_push($dataList, $temp_array);
              }
              foreach($dataList as $dataInfo){              // ループしながら出力
                  mb_convert_variables('SJIS', 'UTF-8', $dataInfo);   //文字コード変換。エクセルで開けるようにする
                  fputcsv($res, $dataInfo);                     //ファイルに書き出しをする
              }

              fclose($res);       // ハンドル閉じる
              header('Content-Description: File Transfer');
              header("Content-Disposition: attachment; filename=workerlist" . date(YmdHis) . ".csv");
              header('Content-Type: application/force-download;');

              ob_clean();
              flush();
              readfile($csvFileName);
              exit;
          }catch(Exception $e) {
              echo $e->getMessage();
          }
      }

      if(array_key_exists('json',$_GET) && $_GET['json']=="1"){    //jsonファイルのエクスポート処理
        try{
            $jsonFileName = '/tmp/' . time() .rand() . 'json';
            $res = fopen($jsonFileName,'w');                      //ファイルを書き込みでオープンする
            if($res === FALSE){
                $err_msg = 'ファイルの書き込みに失敗しました。';
            }
            $dataList = array();
            foreach($result as $row){
                $temp_array = array('社員番号'=>$row['member_ID'],'名前'=>$row['name'],'年齢'=>$row['age'],'性別'=>$gender_array[$row['seibetu']],
                                    '出身'=>$pref_array[$row['pref']],'役職'=>$row['grade_name'],'部署'=>$row['section_name']);
                array_push($dataList, $temp_array);
            }                                                     //連想配列で書きこむ

//            var_dump($temp_array);

            fwrite($res,json_encode($dataList, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE ));
            echo $res;

            fclose($res);
            header('Content-Description: File Transfer');
            header("Content-Disposition: attachment; filename=workerlist" . date(YmdHis) . ".json");
            header('Content-Type: application/force-download;');

            ob_clean();
            flush();

            readfile($jsonFileName);
            exit;
        }catch(Exception $e){
            echo $e ->getmessage();         //エラー表示
        }
      }
      echo "<br />";
      echo "<div id=result_rayout>";
      echo "検索結果：" . count($result) . "件";

      echo "<table id=search_result align='center' class='table table-bordered table-striped'>";  //検索結果表示テーブル
      echo "<thead class='thead-dark'>";
      echo"<tr>
            <th>社員ID</th>
            <th>名前</th>
            <th>部署</th>
            <th>役職</th>
            </tr></thead>";

      if( 0 == count($result)){
        echo "<tr><td colspan ='4'> 検索結果が0件です。</td></tr>";
      }else{
        foreach($result as $each){
          echo "<tr><td>" . $each['member_ID']
                          . "</td><td>"
                          . "<a href = './detail01.php?member_ID="
                          . $each['member_ID']
                          . "'>"
                          . $each['name']
                          . "</a></td><td>"
                          . $each['section_name']
                          . "</td><td>"
                          . $each['grade_name']
                          . "</td></tr>";
        }//出力部分
      }
      echo "</table>";
      echo "</div>"
    ?>
    <!-- CSVエクスポートボタン -->
    <div id = "csv">
      <div id ="c_export">
        <form method="get" action="">
          <input type='hidden' name='csv' value='1'>
          <input type="submit" value="csvエクスポート">
        </form>
      </div>
      <!-- csvインポートボタン -->
        <form method="post" action=importcsv.php enctype="multipart/form-data">
          <input type="file" name="csvfile">
          <input type="submit" onClick="generatecsv();" value="csvインポート">
        </form>
    </div>
    <br>
    <!-- jsonエクスポートボタン -->
    <div id = "json">
      <div id = "j_export">
        <form method="get" action="">
          <input type='hidden' name='json' value='1'>
          <input type="submit" value="jsonエクスポート">
        </form>
      </div>
      <!-- jsonインポートボタン -->
      <form method="post" action=importjson.php enctype="multipart/form-data">
        <input type="file" name="jsonfile">
        <input type="submit" value="jsonインポート">
      </form>
    </div>
  </body>
  </div>
</html>
