
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>第二回課題、フォーム部品練習</title>
  </head>
  <body>
    <h1>消費税計算ページ</h1>
      <form method="get" action="">
        <table border="1">
          <tr>
            <td><b>商品名</b></td>
            <td><b>価格（単位：円、税抜き）</b></td>
            <td><b>個数</b></td>
            <td><b>税率</b></td>
          </tr>
          <tr>
            <td><input type="text" name="syohin01" size="15" maxlength="10"</td>
            <td><input type="text" name="kakaku01" size="15" maxlength="10"</td>
            <td><input type="text" name="kosu01" size="5" maxlength="3">個</td>
            <td>
              <input type="radio" name="tax01" value="1.08">0.8%
              <input type="radio" name="tax01" value="1.1">10%
            </td>
          </tr>
          <tr>
            <td><input type="text" name="syohin02" size="15" maxlength="10"</td>
            <td><input type="text" name="kakaku02" size="15" maxlength="10"</td>
            <td><input type="text" name="kosu02" size="5" maxlength="3">個</td>
            <td>
              <input type="radio" name="tax02" value="1.08">0.8%
              <input type="radio" name="tax02" value="1.1">10%
            </td>
          </tr>
          <tr>
            <td><input type="text" name="syohin03" size="15" maxlength="10"</td>
            <td><input type="text" name="kakaku03" size="15" maxlength="10"</td>
            <td><input type="text" name="kosu03" size="5" maxlength="3">個</td>
            <td>
              <input type="radio" name="tax03" value="1.08">0.8%
              <input type="radio" name="tax03" value="1.1">10%
            </td>
          </tr>
          <tr>
            <td><input type="text" name="syohin04" size="15" maxlength="10"</td>
            <td><input type="text" name="kakaku04" size="15" maxlength="10"</td>
            <td><input type="text" name="kosu04" size="5" maxlength="3">個</td>
            <td>
              <input type="radio" name="tax04" value="1.08">0.8%
              <input type="radio" name="tax04" value="1.1">10%
            </td>
          </tr>
          <tr>
            <td><input type="text" name="syohin05" size="15" maxlength="10"</td>
            <td><input type="text" name="kakaku05" size="15" maxlength="10"</td>
            <td><input type="text" name="kosu05" size="5" maxlength="3">個</td>
            <td>
              <input type="radio" name="tax05" value="1.08">0.8%
              <input type="radio" name="tax05" value="1.1">10%
            </td>
          </tr>

        </table>
        <br>
      <input type="submit" value="送信">
      <input type="reset" value="取り消し">
      </form>

      <?php
      echo "<table border='1'>";
      echo "<tr>
          <td><b>商品名</b></td>
          <td><b>価格（単位：円、税抜き）</b></td>
          <td><b>個数</b></td>
          <td><b>税率</b></td>
          <td><b>小計（単位：円）</b></td>
          </tr>";

      $tax=$_GET['tax01'];
      $kakaku=$_GET['kakaku01'];
      $price01=$kakaku * $tax;
      $gokei=0;
      $gokei=$gokei + $price01;
      if($tax==1.08){
        $tax=0.8;
      }else{
        $tax=10;
      }
      echo "<tr>
          <td>" . $_GET['syohin01'] . "</td>
          <td>" . $_GET['kakaku01'] . "</td>
          <td>" . $_GET['kosu01'] . "</td>
          <td>" . $tax . "%</td>
          <td>" . $price01 . "</td></tr>";

      $tax=$_GET['tax02'];
      $kakaku=$_GET['kakaku02'];
      $price02=$kakaku * $tax;
      $gokei=$gokei + $price02;
      if($tax==1.08){
        $tax=0.8;
      }else{
        $tax=10;
      }
      echo "<tr>
          <td>" . $_GET['syohin02'] . "</td>
          <td>" . $_GET['kakaku02'] . "</td>
          <td>" . $_GET['kosu02'] . "</td>
          <td>" . $tax . "%</td>
          <td>" . $price02 . "</td></tr>";

      $tax=$_GET['tax03'];
      $kakaku=$_GET['kakaku03'];
      $price03=$kakaku * $tax;
      $gokei=$gokei + $price03;
      if($tax==1.08){
        $tax=0.8;
      }else{
        $tax=10;
      }
      echo "<tr>
          <td>" . $_GET['syohin03'] . "</td>
          <td>" . $_GET['kakaku03'] . "</td>
          <td>" . $_GET['kosu03'] . "</td>
          <td>" . $tax . "%</td>
          <td>" . $price03 . "</td></tr>";

      $tax=$_GET['tax04'];
      $kakaku=$_GET['kakaku04'];
      $price04=$kakaku * $tax;
      $gokei=$gokei + $price04;
      if($tax==1.08){
        $tax=0.8;
      }else{
        $tax=10;
      }
      echo "<tr>
          <td>" . $_GET['syohin04'] . "</td>
          <td>" . $_GET['kakaku04'] . "</td>
          <td>" . $_GET['kosu04'] . "</td>
          <td>" . $tax . "%</td>
          <td>" . $price04 . "</td></tr>";

      $tax=$_GET['tax05'];
      $kakaku=$_GET['kakaku05'];
      $price05=$kakaku * $tax;
      $gokei=$gokei + $price05;
      if($tax==1.08){
        $tax=0.8;
      }else{
        $tax=10;
      }
      echo "<tr>
          <td>" . $_GET['syohin05'] . "</td>
          <td>" . $_GET['kakaku05'] . "</td>
          <td>" . $_GET['kosu05'] . "</td>
          <td>" . $tax . "%</td>
          <td>" . $price05 . "</td></tr>";

      echo "<tr>
          <td colspan='4'>合計</td>
          <td>".$gokei."</td>";
      echo "</table>";
    ?>
  </body>
</html>
