
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>配列練習</title>
  </head>
  <body>
    <h1>配列練習ページ</h1>
      <?php

      $team_a = array("リオレウス","リオレイア","ディアブロス","グラビモス");
      $team_b = array("ランポス","ギアノス","イーオス","ゲノポス");
      $team_c = array("ガノトトス","ヴォルガノス","ブラントドス","ジュラトドス");
      $team_d = array("ウラガンキン","イビルジョー","ブラキディオス","ディノバルド");
      $team_e = array("テオ・テスカトル","オオナズチ","クシャルダオラ","ナナ・テスカトル");

      $team_all = array($team_a,$team_b,$team_c,$team_d,$team_e);
?>
<pre>
  <?php
      var_dump($team_all);
  ?>
</pre>
<?php
      echo "<table border='1'>";
      foreach($team_all as $each){
        echo "<tr>";
        foreach ($each as $key => $value) {
          echo "<td>";
          echo $key . ":" . $value;
          echo "</td>";
        }
        echo "</tr>";
      }
      echo "</table>";
      ?>
  </body>
</html>
