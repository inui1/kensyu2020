<?php

function getDB()  //DBに接続する関数
{
  include('./include/statics.php');

  $pdo = new PDO($DB_DNS,$DB_USER,$DB_PW);

  return $pdo;
}


function getSection(){ //部署名をDBから取ってくる関数
  include('./include/statics.php');

  $pdo = new PDO($DB_DNS,$DB_USER,$DB_PW); //DB作成

  $query_str ="SELECT * FROM section1_master ORDER BY section1_master.ID";//部署名取得
  $sql = $pdo->prepare($query_str);
  $sql -> execute();
  $result = $sql->fetchAll();

  return $result;//結果を返す
}

function getGrade(){ //部署名をDBから取ってくる関数
  include('./include/statics.php');

  $pdo = new PDO($DB_DNS,$DB_USER,$DB_PW); //DB作成

  $query_str ="SELECT * FROM grade_master ORDER BY grade_master.ID";//部署名取得
  $sql = $pdo->prepare($query_str);
  $sql -> execute();
  $result = $sql->fetchAll();

  return $result;//結果を返す
}


?>
