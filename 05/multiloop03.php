
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>配列練習</title>
  </head>
  <body>
    <h1>配列練習ページ</h1>
      <?php
        $monster01 = array(
          "id" => "飛竜種",
          "name" => "レオレウス",
          "another_name" => "火竜",
          "type" => "火属性",
          "week" => "雷属性",
          "material" => "火竜の鱗",
          "field" => "丘"
        );
      $monster02 =  array(
        "id" => "飛竜種",
        "name" => "ナルガクルガ",
        "another_name" => "迅竜",
        "type" => "無属性",
        "week" => "火属性",
        "material" => "迅竜の剛翼刃",
        "field" => "森"
        );
      $monster03 =  array(
        "id" => "牙竜種",
        "name" => "ドドブランゴ",
        "another_name" => "雪獅子",
        "type" => "氷属性",
        "week" => "火属性",
        "material" => "雪獅子の剛毛",
        "field" => "雪山"
        );
        $monster04 =  array(
          "id" => "古竜種",
          "name" => "テオ・テスカトル",
          "another_name" => "炎王龍",
          "type" => "爆破属性",
          "week" => "龍属性",
          "material" => "古龍の大宝玉",
          "field" => "火山"
          );

      $monsters = array($monster01,$monster02,$monster03,$monster04);

      echo "<table border='1'>";
      echo "<tr bgcolor='#9999FF'>
            <th>種別</th>
            <th>名前</th>
            <th>別名</th>
            <th>属性</th>
            <th>弱点属性</th>
            <th>素材</th>
            <th>出現場所</th>
            </tr>";

      foreach($monsters as $each){
        echo "<tr><td>" . $each['id'] ."</td><td>"
            . $each['name'] . "</td><td>"
            . $each['another_name'] . "</td><td>"
            . $each['type'] . "</td><td>"
            . $each['week'] . "</td><td>"
            . $each['material'] . "</td><td>"
            . $each['field'] . "</td></tr>";
        }
        echo "</tr>";
      echo "</table>";
      ?>
  </body>
</html>
