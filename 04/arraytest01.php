
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>配列練習</title>
  </head>
  <body>
    <h1>配列練習ページ</h1>
      <?php
        $wepon = array("大剣","太刀","片手剣","双剣","ランス","ガンランス","スラッシュアックス","チャージアックス","操蟲棍");
        echo $wepon[2];
        echo "<br>";
        echo $wepon[0];

        $wepon[9] = "穿龍棍";

      ?>
      <pre>
      <?php var_dump($wepon); ?>
      </pre>
  </body>
</html>
