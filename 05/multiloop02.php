
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>配列練習</title>
  </head>
  <body>
    <h1>配列練習ページ</h1>
      <?php
        $player01 = array(
          "id" => "3",
          "name" => "梶谷隆幸",
          "position" => "外野手",
          "from" => "島根",
          "year" => "2007",
        );
      $player02 =  array(
          "id" => "44",
          "name" => "佐野恵太",
          "position" => "外野手",
          "from" => "岡山",
          "year" => "2017",
        );
      $player03 =  array(
          "id" => "15",
          "name" => "井納翔一",
          "position" => "投手",
          "from" => "東京",
          "year" => "2013",
        );

      $players = array($player01,$player02,$player03);
?>
<pre>
  <?php
      var_dump($players);
  ?>
</pre>
<?php
      echo "<table border='1'>";
      echo "<tr>
            <th>id</th>
            <th>name</th>
            <th>position</th>
            <th>from</th>
            <th>year</th>
            </tr>";

      foreach($players as $each){
        echo "<tr>";
        foreach ($each as $value) {
          echo "<td>";
          echo $value;
          echo "</td>";
        }
        echo "</tr>";
      }
      echo "</table>";
      ?>
  </body>
</html>
